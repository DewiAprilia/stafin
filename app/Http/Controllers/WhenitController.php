<?php

namespace App\Http\Controllers;

use App\Models\WhenitModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class WhenitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('whenit', ['event' => WhenitModel::all(), 'today' => Carbon::now()->toDateString()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $whenit = new WhenitModel;
        $whenit->id = $this->randString(64);
        $whenit->account = $this->randString(64);
        $whenit->title = $request->title;
        $whenit->start = $request->start;
        $whenit->end = $request->end;
        $whenit->desc = $request->desc;

        $whenit->save();
        return redirect('/whenit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WhenitModel $whenit)
    {
        $validate = $request->validate([
            'title' => "required",
            'start' => "required",
            'end' => "required",
            'desc' => ""
        ]);
        $whenit->update($validate);
        return redirect('/whenit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(WhenitModel $whenit)
    {
        $whenit->delete();
        return redirect('/whenit');
    }
}
