@extends('layouts.template')

@section('tabTitle', 'When It')
@section('title', 'When It')
@section('whenit', 'cl0')

@section('left')
@endsection

@section('left1')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">New Event</h2>
        <form action="/whenit" method="POST">
            @csrf
            <input type="text" style="outline:none" class="border-0 r20 bg2 px-4 py-3 me-4 fs-5 wht9 f600 w-100"
                placeholder="Title" name="title">
            <div class="d-flex mt-4">
                <span class="f600 wht6 fs-6 align-self-center px-3">From</span>
                <input type="date" class="f600 bg2 border-0 r20 wht6 px-4 py-3 w-100" placeholder="Start" name="start">
                <span class="f600 wht6 fs-6 align-self-center px-3">to</span>
                <input type="date" class="f600 bg2 border-0 r20 wht6 px-4 py-3 w-100" placeholder="End" name="end">
            </div>
            <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3" placeholder="Description"
                name="desc"></textarea>
            <div class="d-flex mb-3">
                <button type="submit" class="f600 shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add Event</button>
                <button type="reset" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3 ">Clear</button>
            </div>
        </form>
    </x-card>
@endsection

@section('right')
    <h2 class="ps-4 py-2 mt-5 mb-4 fs-4 wht9 f600">Today's Events</h2>
    @forelse ($event as $ev)
        @if ($ev->status == 1 && explode(' ', $ev->summary)[0] == $today)
            <x-card class="me-3 mb-5" :shadow="true" style="secondary">
                <div class="px-3 py-4">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                        </div>
                        <p class="align-self-center m-0 f600 fs-7 wht6">
                            Vote Completed
                        </p>
                    </div>
                    <div class="pt-3">
                        <p class="fs-5 cl0 f600 mb-0">
                            {{ $ev->title }}
                        </p>
                        <p class="fs-7 wht6 f600 text">
                            {{ $ev->desc }}
                        </p>
                        <div class="d-flex justify-content-end">
                            <p class="mb-0 fs-7 cl0 f600 ">
                                {{ $ev->summary }}
                            </p>
                        </div>
                    </div>
                </div>
            </x-card>
        @endif
    @empty
        <x-card class="me-3 mb-5" :shadow="true" style="secondary">
            <h2 class="ps-4 py-5 text-center m-0 fs-5 wht9 f600">No Events Today</h2>
        </x-card>
    @endforelse
@endsection

@section('bottom')
    <section class="pt-5 ">
        <h2 class="ps-5 py-2 fs-4 wht9 f600">Event List</h2>
        <section class="row py-5">
            <p class="wht8 fs-6 f600 ps-5 mb-1 ms-2">Vote Completed</p>
            @forelse ($event as $ev)
                @if ($ev->status == 0)
                    @continue
                @endif
                <div class="col-4">
                    <div data-bs-toggle="modal" data-bs-target="#mainModal"
                        onclick="setData({{ Illuminate\Support\Js::from($ev) }})">
                        <x-card class="me-3 mb-4" :shadow="true" style="secondary">
                            <div class="px-3 py-4">
                                <div class="d-flex">
                                    <div class="align-self-center rounded-circle me-3"
                                        style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                                    </div>
                                    <p class="align-self-center m-0 f600 fs-7 wht6">
                                        Vote Completed
                                    </p>
                                </div>
                                <div class="pt-3">
                                    <p class="fs-5 cl0 f600 mb-0">
                                        {{ $ev->title }}
                                    </p>
                                    <p class="fs-7 wht6 f600 text">
                                        {{ $ev->desc }}
                                    </p>
                                    <div class="d-flex justify-content-end">
                                        <p class="mb-0 fs-7 @if (explode(' ', $ev->summary)[0] == $today) cl0 @else wht9 @endif f600 ">
                                            {{ $ev->summary }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </x-card>
                    </div>
                </div>
            @empty
                <x-card class="me-3 mb-5" :shadow="true" style="secondary">
                    <h2 class="ps-4 py-5 text-center m-0 fs-5 wht9 f600">No Events</h2>
                </x-card>
            @endforelse
        </section>
        <section class="row py-5">
            <p class="wht8 fs-6 f600 ps-5 mb-1 ms-2">Vote Uncompleted</p>
            @forelse ($event as $ev)
                @if ($ev->status == 1)
                    @continue
                @endif
                <div class="col-4">
                    <div data-bs-toggle="modal" data-bs-target="#mainModal"
                        onclick="setData({{ Illuminate\Support\Js::from($ev) }})">
                        <x-card class="me-3 mb-4" :shadow="true" style="secondary">
                            <div class="px-3 py-4">
                                <div class="d-flex">
                                    <div class="align-self-center rounded-circle me-3"
                                        style="width:1rem; height:1rem; border:3px solid var(--cl1)">
                                    </div>
                                    <p class="align-self-center m-0 f600 fs-7 wht6">
                                        Vote Uncompleted
                                    </p>
                                </div>
                                <div class="pt-3">
                                    <p class="fs-5 cl1 f600 mb-0">
                                        {{ $ev->title }}
                                    </p>
                                    <p class="fs-7 wht6 f600 text">
                                        {{ $ev->desc }}
                                    </p>
                                </div>
                            </div>
                        </x-card>
                    </div>
                </div>
            @empty
                <x-card class="me-3 mb-5" :shadow="true" style="secondary">
                    <h2 class="ps-4 py-5 text-center m-0 fs-5 wht9 f600">No Events</h2>
                </x-card>
            @endforelse
        </section>
        {{-- <section class="row py-5">
            <p class="wht8 fs-6 f600 ps-5 mb-1 ms-2">Past</p>
            @if (explode(' ', $ev->summary)[0] < $today || $ev->end < $today)
            @endif
            @forelse ($event as $ev)
                <div class="col-4">
                    <x-card class="me-3 mb-4" :shadow="true" style="secondary">
                        <div class="px-3 py-4">
                            <div class="d-flex">
                                <div class="align-self-center rounded-circle me-3"
                                    style="width:1rem; height:1rem; border:3px solid @if ($ev->status) var(--cl0) @else var(--cl1) @endif">
                                </div>
                                <p class="align-self-center m-0 f600 fs-7 wht6">
                                    @if ($ev->status)
                                        Vote Completed
                                    @else
                                        Vote Uncompleted
                                    @endif
                                </p>
                            </div>
                            <div class="pt-3">
                                <p class="fs-5 wht6 f600 mb-0">
                                    {{ $ev->title }}
                                </p>
                                <p class="fs-7 wht6 f600 text">
                                    {{ $ev->desc }}
                                </p>
                                <div class="d-flex justify-content-end">
                                    <p class="mb-0 fs-7 wht6 f600 ">
                                        @if ($ev->status)
                                            {{ $ev->summary }}
                                        @else
                                            ????-??-?? at ??:??:??
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </x-card>
                </div>
            @empty
                <x-card class="me-3 mb-5" :shadow="true" style="secondary">
                    <h2 class="ps-4 py-5 text-center m-0 fs-5 wht9 f600">No Events</h2>
                </x-card>
            @endforelse
        </section> --}}
    </section>
@endsection

@section('modal')
    <div class="modal fade" id="mainModal" tabindex="-1" aria-labelledby="mainModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background:transparent;">
                <x-card :shadow="true" style="primary">
                    <div class="d-flex justify-content-between mb-4">
                        <h5 class="ps-4 py-2 fs-5 wht9 f600 modal-title">Event Detail</h5>
                        <form action="/whenit/destroy" id="deleteForm" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="align-self-center btn m-0 p-2 cl1 f600 fs-6"
                                onclick="return confirm('Are you sure to delete this event ?')"><span
                                    class="bi bi-trash"></span></button>
                        </form>
                    </div>
                    <form action="/whenit" id="updateForm" method="POST">
                        @csrf
                        @method('PATCH')
                        <input type="text" style="outline:none" class="border-0 r20 bg2 px-4 py-3 me-4 fs-5 wht9 f600 w-100"
                            placeholder="Title" name="title" id="vTitle">
                        <div class="d-flex mt-4">
                            <span class="f600 wht6 fs-6 align-self-center px-3">From</span>
                            <input type="date" class="f600 bg2 border-0 r20 wht6 px-4 py-3 w-100" placeholder="Start"
                                name="start" id="vStart">
                            <span class="f600 wht6 fs-6 align-self-center px-3">to</span>
                            <input type="date" class="f600 bg2 border-0 r20 wht6 px-4 py-3 w-100" placeholder="End"
                                name="end" id="vEnd">
                        </div>
                        <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3" placeholder="Description"
                            name="desc" id="vDesc"></textarea>
                        <div class="d-flex mb-3">
                            <button type="submit" class="f600 shdw bg2 border-0 r20 wht9 px-5 py-3 ">Update Event</button>
                            <button type="reset" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3"
                                data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </x-card>
            </div>
        </div>
    </div>
    <script>
        const setData = (data) => {
            $('#vTitle').val(data.title);
            $('#vStatus').val(data.status);
            $('#vStart').val(data.start);
            $('#vEnd').val(data.end);
            $('#vSummary').val(data.summary);
            $('#vDesc').val(data.desc);
            $("#updateForm").attr('action', '/whenit/' + data.id);
            $("#deleteForm").attr('action', '/whenit/' + data.id);
        }
    </script>
@endsection
