<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>@yield('tabTitle')</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body class="bg0">
    <nav class="shdw bg1 position-fixed w-auto px-4 py-4 d-flex flex-column justify-content-between"
        style="height: 100vh; top: 0; left: 0;">
        <div class="d-flex flex-column px-1 pt-4">
            <a class="py-2" href="/"><span
                    class=" bi bi-columns-gap fs-4 @section('dashboard') cl2 @show"></span></a>
            <a class="py-2" href="/money"><span
                    class=" bi bi-wallet fs-4 @section('money') cl2 @show"></span></a>
            <a class="py-2" href="/todo"><span
                    class=" bi bi-card-list fs-4 @section('todo') cl2 @show"></span></a>
            <a class="py-2" href="/whenit"><span
                    class=" bi bi-calendar-date fs-4 @section('whenit') cl2 @show"></span></a>
        </div>
        <div class="d-flex flex-column px-1">
            <a class="py-2" href="/"><span
                    class=" bi bi-person-circle fs-4 @section('profile') cl2 @show"></span></a>
            <a class="py-2" href="/"><span class=" bi bi-gear fs-4 @section('settings') cl2 @show"></span></a>
            <a class="py-2" href="/"><span class=" bi bi-box-arrow-left fs-4 cl2"
                    style="margin-left: -0.15em"></span></a>
        </div>
    </nav>

    <main class="container py-5 ps-5 pe-0">
        <div class="row">
            <div class="col-6">
                <div class="px-5 pb-4">
                    <h1 class="f600 wht9 mb-5">@yield('title')</h1>
                    @yield('left')
                </div>
                @yield('left1')
            </div>
            <div class="col-6 pt-4 ps-5">
                @yield('right')
            </div>
            <div class="col-12 pt-4">
                @yield('bottom')
            </div>
        </div>
    </main>
    @yield('modal')
</body>

</html>
